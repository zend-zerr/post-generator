$(document).ready(function() {
	$('.search-videos').click(function() {
		var query = $('input[name=videos]').val();
		jQuery.ajax({
		  	url: '../wp-content/plugins/post-generator/services/Youtube.php',
		  	beforeSend: function() {
				$('.video-results').html('');
		  	},
		  	data: { query },
		  	dataType: 'json',
		  	type: 'GET',
		  	success:function(data){
		  		try {
			  		var tr = '';
			  		data.forEach(function(video, i) {
			  			tr += 
			  			'<tr>'+
			  				'<td><input type="checkbox" class="id_video" name="id_video" value="'+video.id+'"></td>'+
			  				'<td><iframe width="200" height="150" src="'+video.embed+'"></iframe></td>'+
			  				'<td><h2><b>'+video.title+'<b><h2></td>'+
			  			'</tr>';
			  		});
			  		var table = '<table>'+tr+'</table>';
			  		$('.video-results').append(table);
		  		} catch(err) {console.log(err)}
		  	}
		});
	});
});