<?php

class VideoSpintax {

	private $videos;

	public function __construct(array $videos) {
		
        $this->videos = $videos;
	}

	public function process($text)
    {
        return preg_replace_callback(
            '/\(([^\)]*)\)/',
            array($this, 'replace'),
            $text
        );
    }

    public function replace($text)
    {   
        $totalVideos = count($this->videos);
        $randomId = rand(0, $totalVideos - 1);
        $videos = isset($this->videos[$randomId]) ? $this->videos[$randomId] : '';

        $text = $this->process($text[1]);
        $parts = explode('|', $text);
        $embed = '';

        $mark = isset($parts[0]) ? $parts[0] : '';
        $width = isset($parts[1]) ? $parts[1] : 300;
        $height = isset($parts[2]) ? $parts[2] : 200;

        if ($parts[0] == 'video') {
            $embed = '<br><iframe width="'.$width.'" height="'.$height.'"
    					src="https://www.youtube.com/embed/'.$videos.'">
    				</iframe><br>';
        }

        if ($totalVideos == 0) {
            $embed = '';
        }
        return $embed;

    }

}