<?php 

/**
 * Author: Vatih team
 * Version: 0.1
 * Description: scrape and search youtube videos in firstpage
 * License: GPLv2 or later
 */
class Youtube {

	public function request($query) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,'https://www.youtube.com/results');
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,	
		            'search_query='.$query);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$server_output = curl_exec ($ch);
		curl_close ($ch);

		return $this->loadHtml($server_output, $query);
	}

	public function loadHtml($html) {
		$dom = new DOMDocument;
		@$dom->loadHTML($html);
		$finder = new DomXPath($dom);
		$classname = 'yt-uix-tile-link';
		$links = $finder->query("//*[contains(@class, '$classname')]");
		$arrLinks = array();
		foreach ($links as $link){
			$idVideo = substr($link->getAttribute('href'), 9);
			$embedVideo = 'https://www.youtube.com/embed/'.$idVideo;
			$data = array(
				'id' => $idVideo,
				'embed' => $embedVideo,
				'title' => $link->nodeValue
			);
			array_push($arrLinks, $data);
		}
		return $arrLinks;
	}
}

$youtube = new Youtube();
$query = $_GET['query'];
$search = $youtube->request($query);
echo json_encode($search);