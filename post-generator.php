<?php
/*
Plugin Name: Post generator
Description: Post generator
Version: 0.1
Author: Akhmad Romadlon Zainur Rofiq & Widada
Author URI: http://muslimevents.id
Text Domain: Post generator
License: GPLv2 or later
*/

// Gaboleh akses file langsung
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// fungsi yang kita panggil ketika plugin di install
register_activation_hook( __FILE__, 'backlink_management_install' );

// fungsi install
// disini kita bisa bikin tabel, dll.
// terserah mau ngapain pas plugin abis di install
function backlink_management_install() {

}

// nambahin css tambahan
wp_register_style( 'stylish', '/wp-content/plugins/post-generator/css/custom.css');
wp_enqueue_style('stylish');

// add_menu_page( $page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position );
// Nambahin menu di sidebar halaman admin
add_action('admin_menu', 'mt_add_pages');

// add menu
function mt_add_pages() {
	$icon_url = plugin_dir_url( __FILE__ ). 'images/sintesa-admin-icon-new.png';
    add_menu_page(__('Post generator','Post generator'), __('Post generator','menu-test'), 'manage_options', 'post-generator', 'mt_toplevel_page',$icon_url, '23,56' );
}

// fungsi view halaman nya
function mt_toplevel_page() { 
	ob_start();
	?>
	<?php
    	global $wpdb;
    	$authors = $wpdb->get_results("SELECT ID, user_nicename, display_name, user_email from $wpdb->users ORDER BY display_name ASC");
    	
		if (isset($_GET['success']) && $_GET['success'] == 'true') { ?>
	
			<div id='message' class='updated notice notice-success is-dismissible' style='margin: 15px 2px;'>
			<p>Posts have been generated succesfully.</p>
			<button type='button' class='notice-dismiss'><span class='screen-reader-text'>Dismiss this notice.</span></button></div>
	
	<?php } ?>

	<h2 class="page-title">Auto generate post</h2>
	
	<div class="post-form-container">
		<div class="post-form">
			<label>Title</label>
				<input type="text" value="Pancake durian" class="form-control post_title" name="title" placeholder="Post title"><br>
			<label>Prefix</label>
				<input type="text" value="jual,info,harga" class="form-control post_prefix" name="prefix" placeholder="Post prefix"><br>
			<label>Sufix</label>
				<input type="text" value="Semarang,Jakarta,Aceh" class="form-control post_sufix" name="sufix" placeholder="Post sufix"><br>
			<label>Embed Video</label>
				<input type="text" value="durian" class="form-control post_prefix" name="videos" placeholder="Search videos"><br>
				<button class="button button-primary button-large search-videos">search</button>
				<br><br>
				<div class="video-results"></div>
			<label>Content</label>
				<textarea class="form-control post_content" placeholder="Post content">{halo|bro|sis} jual {murah|miring} (video) </textarea><br>
			<button class="button button-primary button-large" id="post">Post</button>
			<!-- <button class="button button-primary button-large">Draft</button> -->
		</div>
		<div class="post-setting">	
			<h2 class="page-title">Setting</h2>
			<label>Post Count</label><br>
			<input type="number" name="post_count" placeholder="Post count here ..." value="1">
			<br><br>
			<label>Authors</label><br>
				<?php 
					foreach($authors as $author) {
						echo "<input type='checkbox' name='authors[]' value='".$author->ID."'>".$author->display_name." ";
					}
				?>
			<br><br>
			<label>Post Time (the difference post time for each post in minute, minimum is 1 max is 60)</label>
			<input type="number" name="post_time" value="1">
		</div>
	</div>
	<div id="loading">
		<div class="cssload-jar">
			<div class="cssload-mouth"></div>
			<div class="cssload-neck"></div>
			<div class="cssload-base">
				<div class="cssload-liquid"> </div>
				<div class="cssload-wave"></div>
				<div class="cssload-wave"></div>
				<div class="cssload-bubble"></div>
				<div class="cssload-bubble"></div>
			</div>
			<div class="cssload-bubble"></div>
			<div class="cssload-bubble"></div>
		</div>
		<h1>Loading ...</h1>
	</div>
	<div class="post-result">
		<ul class="post-results"></ul>
	</div>
	<script type='text/javascript' src='../wp-content/plugins/post-generator/js/jquery-2.1.4.min.js'></script>
<?php
	wp_enqueue_script('autogenerate-process','/wp-content/plugins/post-generator/js/process.js');
	wp_enqueue_script('services/Youtube','/wp-content/plugins/post-generator/js/search_videos.js');
	$page = ob_get_contents();
   	ob_end_clean();
   	echo $page;
}

?>